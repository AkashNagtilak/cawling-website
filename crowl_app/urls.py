from django.urls import path
from .views import DashBoard, SearchDatabaseTag, SearchDatabaseView, SearchTagAjax

urlpatterns = [
    path('', DashBoard.as_view(), name='dashbaord-view'),
    path('search-database-tag/', SearchDatabaseTag.as_view(), name='search-medium-tag'),
    path('search-database/', SearchDatabaseView.as_view(), name= "search-database"),
    path('search-ajax/', SearchTagAjax.as_view(), name= "search-ajax"),
]
