from django.shortcuts import render
from django.views import generic
import requests
from bs4 import BeautifulSoup
from django.conf import settings
import re
from django.db.models import Q
# Manually added
from .models import SearchTagModel
from django.shortcuts import HttpResponse
# Create your views here.
class DashBoard(generic.TemplateView):
    """
        Class for DashBard View.
    """
    template_name = 'crowl_app/index.html'


class SearchDatabaseTag(generic.View):
    model = SearchTagModel
    template_name = 'crowl_app/search_tag.html'
    context_object_name = 'search_tag_medium'

    def get(self, request, *args, **kwargs):

        return render(request, self.template_name)

class SearchDatabaseView(generic.ListView):
    """
        Class for search database.
    """
    model = SearchTagModel
    template_name = "crowl_app/search_tag_database.html"
    paginate_by = 6

    def get_queryset(self):

        data = self.request.GET
        school_search = data.get('school_search')

        search = data.get('search')

        qs = self.model.objects.all().order_by('-date_created')

        if search:
            if search.isnumeric():
                qs = qs.filter(id=int(search))
            else:
                qs = qs.filter(Q(title_text__icontains=search) | Q(autor_list__icontains=search) | Q(
                    article_tags__icontains=search) | Q(minite_read__icontains=search))

        return qs

from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from django.template.loader import render_to_string

@method_decorator(csrf_exempt, name="dispatch")
class SearchTagAjax(generic.View):
    """
        Class for ajax search tag.
    """
    
    def post(self, request, *args, **kwargs):

        data = self.request.POST
        search = data.get('search_value')

        if search:

            context = dict()
            headers = {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET',
                'Access-Control-Allow-Headers': 'Content-Type',
                'Access-Control-Max-Age': '3600',
                'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
            }
            url_link = "https://medium.com/tag/" + str(search)
            result = requests.get(url_link, headers)
            soup = BeautifulSoup(result.content, "html.parser")
            article_title = soup.find_all("h2", {"class": settings.TITALE_TEXT})
            article_paragraph = soup.find_all("p", {"class": settings.ARICLAE_PARAGRAPH})
            minitue_read = soup.find_all("p", {"class": settings.MINIT_READ})
            articles_tags = soup.find_all("div", {"class": settings.ARICLAE_TAGAS})
            author_list = soup.find_all('p', {"class": settings.AUTHOR_LIST})
            author_url = soup.find_all('a', attrs={'href': re.compile("^https://")})
            
            title_text = [''.join(title.findAll(text=True)) for title in article_title],
            paragrph = [''.join(paragraph.findAll(text=True)) for paragraph in article_paragraph],
            article_url = [''.join(url.get('href')) for url in author_url],
            minite_read = [''.join(minitue.findAll(text=True)) for minitue in minitue_read],
            article_tags = [''.join(article.findAll(text=True)) for article in articles_tags],
            author = [''.join(author.findAll(text=True)) for author in author_list]
            # au av aw ax ay az ba bb bc bd be bf bg bh bi

            all_data = list(zip(*title_text, *paragrph, *article_url, *minite_read, *article_tags, author))
            context_list = []

            for obj in all_data:
                context_list.append({
                    "title": obj[0], "tags": obj[4], "creater": obj[-1], "details": obj[1], "minute_read": obj[3], 
                    "url": obj[2]
                })

                SearchTagModel.objects.update_or_create(title_text=obj[0], autor_list=obj[-1], article_paragraph=obj[1], article_url=obj[2], article_tags=obj[4], minite_read=obj[3])

            context.update({
                "context_list": context_list,
            })

            html = render_to_string('crowl_app/item_table.html', context)
            return HttpResponse(html)
        else:
            return HttpResponse(render_to_string('crowl_app/item_table.html', {}))
